# 4. Electronic Prototyping

Purpose of this unit : to learn how to use a microcontroller development board to prototype electronic applications by programming it and using input and output devices.

The assignment given was to : 

- DHT20 - Temperature and Relative Humidity Sensor : 

1) Read the sensor data sheet and identify the relevant features.
2) Connect the sensor to your development board.
3) Write a program to read the data and send it to your computer via usb.
4) Write a program to process the data and display information using the built-in LED.

- Advanced sensor : 

1) Choose an advanced sensor that we are interested in.
2) Measure something : add the sensor to a development board and read it.
3) Read the sensor data sheet and explain how the physical property relates to the measured results.
4) (Optional) Make our board do something : add an output device to a development board and program it to do something.

## 4.1. DHT20 - Temperature and Relative Humidity sensor 

### 4.1.1. Relevant features and informations from the data sheet 

The main information required for the wiring of the DHT20 sensor, taken from its [data sheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf), is shown in the table below : 

|DHT20 Sensor|Important information on the pins and the typical circuit|
|---|---|
|![](images/photo_DHT20.png)|![](images/pins_DHT20.png) ![](images/circuit_DHT20.png)|

### 4.1.2. Connection of the sensor to the development board 

Thanks to the data sheet and mainly to the information in the table above, it is therefore possible to make the following cabling :

![](images/connexion_board1.jpg) ![](images/connexion_board2.jpg)

#### 4.1.2.1. First attempt 

In order to test the temperature and humidity sensor, I used the Arduino program. After installing the library corresponding to the DHT20, I selected the right board and the right port : 

![](images/board_port.png)

The gallery allows to get examples of codes to test the setup. I chose the "DHT20_plotter" example which allows to obtain the values in relative humidity and temperature in a numerical way but also in the form of graph in real time : 

![](images/DHT20_plotter.png)

The example code is as follows:

```
//    FILE: DHT20_plotter.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: Demo for DHT20 I2C humidity & temperature sensor
//  Always check datasheet - front view

//          +--------------+
//  VDD ----| 1            |
//  SDA ----| 2    DHT20   |
//  GND ----| 3            |
//  SCL ----| 4            |
//          +--------------+

#include "DHT20.h"

DHT20 DHT(&Wire);

void setup()
{
  // DHT.begin();  
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();
  //  ESP32 default pins 21 22
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
}

void loop()
{
  if (millis() - DHT.lastRead() >= 1000)
  {
    // note no error checking
    DHT.read();
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(", ");
    Serial.println(DHT.getTemperature(), 1);
  }
}

// -- END OF FILE --

```
As explained during the formation, in order for the example to work it is necessary to change the line "DHT.begin()" by "Wire.setSDA(0); Wire.setSCL(1); Wire.begin();". This is explained in [this link](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/exercices/dht20/). The results obtained after simulation of this code are as follows : 


![](images/résultat_num.png) ![](images/résultats_plot.png)

As we can see on the graph in real time, when we blow on the detector we observe an increase in temperature and humidity. It would now be interesting to show separately an increase in temperature while keeping a low humidity threshold, an increase in humidity while keeping a low temperature threshold and an increase in both simultaneously. In order to study these 3 cases with the help of the LED integrated in the board, I modified the code of the example. This is explained in the section "use of the built-in LED". 

#### 4.1.2.2. Use of the built-in LED 

In order to show the 3 cases explained above, I modified the code of the example by the code below. This one allows to light the LED integrated in the board in 3 different ways: 

- The LED flashes red when the temperature exceeds 26°C. 
- The LED flashes blue when the relative humidity exceeds 70%.
- The LED lights up red continuously when the temperature of 26°C and the RH of 70% are exceeded. 

```
// FILE: DHT20_led.ino
// AUTHOR : Virginie Louckx 
// FROM : Rob Tillaart
// LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
//
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
#include "DHT20.h"

#define LED_PIN     23
#define LED_COUNT  1
#define BRIGHTNESS 50 // Set BRIGHTNESS to about 1/5 (max = 255)
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRBW + NEO_KHZ800);

DHT20 DHT(&Wire);

void setup()
{
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();
  //  ESP32 default pins 21 22
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");

#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  strip.begin();
  strip.show();
  strip.setBrightness(BRIGHTNESS);
  strip.setPixelColor(0, strip.Color(255,   255,   255));
  strip.show();
}

void loop()
{
  DHT.read();
  int i =0;
  while(DHT.getTemperature()>26.0 || DHT.getHumidity()>70.0)
  {
    if (DHT.getTemperature()>26.0 && DHT.getHumidity()<70.0){
    strip.setPixelColor(0, strip.Color(0,   0,   0));
    strip.show();
    delay(100);
    strip.setPixelColor(0, strip.Color(255,   0,   0));
    strip.show();
    delay(100);}

    else if (DHT.getHumidity()>70.0 && DHT.getTemperature()<26.0 ){
    strip.setPixelColor(0, strip.Color(0,   0,   0));
    strip.show();
    delay(100);
    strip.setPixelColor(0, strip.Color(0,   0,   255));
    strip.show();
    delay(100);}

    else {
    strip.setPixelColor(0, strip.Color(255,   0,   0));
    strip.show();
    delay(200);
    }
    
    i++;
    if(i>5){
      i=0;
      DHT.read();  
      Serial.print(DHT.getHumidity(), 1);
      Serial.print(", ");
      Serial.println(DHT.getTemperature(), 1); 
    }    
  }
  
  strip.setPixelColor(0, strip.Color(255,   255,   255));
  strip.show();
  Serial.print(DHT.getHumidity(), 1);
  Serial.print(", ");
  Serial.println(DHT.getTemperature(), 1);
  delay(1000);
}

```
The 3 cases are demonstrated in the following videos. To do this I used : 

|Temperature threshold test only (> 26°C - LED flashes red) |RH threshold test only  (> 70% - LED flashes blue) |Test of both  (> 26°C & > 70% - LED lights up red continuously)|
|---|---|---|
|Air dryer|Cold blow (with mouth)|Hot blow (with mouth)|
|<iframe src="../images/over_26.mp4" width="200" height="150" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe> |<iframe src="../images/over_70.mp4" width="200" height="150" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe> |<iframe src="../images/both.mp4" width="200" height="150" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe> |


This setup can be interesting to know the humidity and temperature of a room. Indeed, the ambient temperature generally desired in a house is around 20°C and the relative humidity of a healthy environment is between 40 and 70%. Beyond that, the environment can become propitious to the installation of moulds or fungi. 

## 4.2. Advanced sensor : pulse sensor 

### 4.2.1. Relevant features and informations from the data sheet 

As a choice, I opted for the pulse sensor and wanted to make it work with mycropython this time. Since the information about it is not available on the course sites, I found the following documentation and useful links on the internet : 

- For information about the technical specifications and functioning of the sensor : [this link](https://lastminuteengineers.com/pulse-sensor-arduino-tutorial/#:~:text=The%20Pulse%20Sensor%20is%20a,developers%E2%80%94can%20benefit%20from%20it.). 
- For information on how to run the sensor with micropython : [this link](https://peppe8o.com/pulse-sensor-with-raspberry-pi-pico-hearth-beat-chech-with-micropython/). 

As explained in the [useful link](https://peppe8o.com/pulse-sensor-with-raspberry-pi-pico-hearth-beat-chech-with-micropython/) above, this sensor is an optical heart rate sensor that works by projecting green light (~ 550 nm) onto the finger and measuring the amount of reflected light using a photosensor. 

The oxygenated hemoglobin in arterial blood absorbs the green light. So the redder the blood, the higher the hemoglobin and the greater the absorption of green light. With each heartbeat, blood is pumped into the finger, which causes a change in the amount of reflected light, which in turn produces a waveform at the output of the photosensor. The measurements from the photosensor therefore tell us a measure of the heartbeat.

### 4.2.2. Connection with the Rasberry Pi Pico RP2040

As explained in the datasheet of the sensor, the chip is powered with a 3.3V supply and is connected to an ADC of the Rasberry Pi Pico. The input of the ADC is a voltage between 0 and 3.3V and it correponds to the amount of the reflected light as explained in 4.2.1. This voltage is converted by the ADC into a raw value of 12 bits. This value is filtered and used in the code to detect the heartbeats as explained in section 4.2.3. 

### 4.2.3. Measurment of the heartbeat and use of an outpout device 

I connected the sensor to the board according to the information I found on the internet and stuck it with adhesive paper on one of my fingers to avoid it to move :

![](images/set_up.jpg)

The little finger gave better results than the other fingers because the skin is thinner there. 

My objective for the use of this sensor was to measure a pulse and to make a led blink at the rhythm of this pulse. To do this, I modified the code from the [useful link](https://peppe8o.compulse-sensor-with-raspberry-pi-pico-hearth-beat-chech-with-micropython/) and used the code below : 

```
# FILE : heart_beat_test
# AUTHOR : Virginie Louckx 
# FROM : https://peppe8o.com/pulse-sensor-with-raspberry-pi-pico-hearth-beat-chech-with-micropython/
#LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)


from machine import Pin, ADC            #importing Pin and ADC class
from time import sleep_ms #importing sleep class
import neopixel

np = neopixel.NeoPixel(Pin(23), 1)
pulse = ADC(26)           #creating potentiometer object

file = open("Avg.txt", "w")
file2 = open("thresh.txt", "w")


x=0
max_samples = 1000
short_average=35
long_average=300
beat_threshold=-50
finger_threshold=1000000
history = []
hist_avg = []
avg=0
# main program

def finger_detected(beat_thresh):
    avg_1=sum(history[-short_average:])/short_average
    avg_2=sum(history[-long_average:])/long_average
    avg=avg_1-avg_2
    
    if avg > beat_thresh:
        np[0] = (255, 0, 0)
    else:
        np[0] = (0, 0, 0)
    np.write()
    return avg

while x<3000:
    x=x+1
    #if x%100==0:
        #print(beat_threshold)
        #print(x)
    try:
        value=pulse.read_u16()
        #file.write(str(value)+"\n")
        history.append(value)
        history = history[-max_samples:]
        hist_avg.append(avg)
        hist_avg = hist_avg[-100:]
        if max(history)-min(history) < finger_threshold:
            beat_threshold = (max(hist_avg))*0.5
            avg=finger_detected(beat_threshold)
            file.write(str(avg)+"\n")
            file2.write(str(beat_threshold)+"\n")
              
        else:
            np[0] = (0, 0, 0)
        np.write()
    except OSError as e:
        machine.reset()

```

### 4.2.4. Explanation of the code : How do I get the LED to light up with each beat?

To do this, I used the values obtained with the micropython code and put them in Excel. This way I could get a graph of the heartbeats according to the samples of the heartbeats. 

The first thing I had to do was to smooth the signal obtained. Indeed, since this one was very noisy, I used a sliding average of 35 samples to smooth the signal. I then subtracted the average from the signal to center it around zero using a sliding average of 300 samples. Once the signal was readable, I had to create a variable threshold to detect each heartbeat. It is this threshold that will switch on the LED integrated in the board at each beat. 

![](images/graphe_heart.png)

It is therefore possible to measure the heartbeat of someone and to visualize it thanks to the integrated LED. The video below shows how the device works :

<iframe src="../images/heart.mp4" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe> 


