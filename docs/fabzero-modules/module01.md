# 1. Documentation

## 1.1. First class

During the first class, we had the opportunity to get to know and break the ice with students from various faculties such as polytechnic, computer science, biology etc. This was very enriching and allowed me to dare to communicate more easily and ask for help afterwards. 

## 1.2. Second Class

For the second class session we were given an assignment which was : 

* Build a personal site in the class archive that describes us.
* Document our learning path for this module.

This was done with the help of Gitlab which is a version control software that allows us to keep track of our work and learning while making this documentation available to other students.

### 1.2.1. SSH keys 

For this first assignment, we were advised to start by connecting our local computer to the remote server in a secure way using SSH keys. The steps were as follows :

1) Install GitLab 
2) Install Git Bash 
3) Configure Git
4) Configure SSH keys 

For the first 3 steps, I followed the path of [this tutorial](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#version-control-git-gitlab).  

The last step was particularly difficult, I had never done this before. After 2 hours of class I still didn't have the SSH keys. But I was not far, I just had to follow the path described in this section : [How to generate an SSH key pair](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair). 

I finally managed to get the keys. I then introduced one of the them in the SSH keys tab of GitLab and pasted the content : 

![](images/SSH_section.png)

An error message appeared: 

![](images/msg_erreur.png)  

This meant that I had entered the wrong key (the personal key that should not be shared!). So I entered the other one and that was it! 

### 1.2.2. Cloning of the project 

The connection being secured, I could cloned my project on my computer to work directly on the local version. To do so, I first had to make sure I was in the right work directory :

![](images/cd.png) 

Indeed, it is possible to use the _dir_ command to list the folders in the directory and _cd_ to change the working directory. Once I was in the right working directory, I used the _git clone_ command and the URL from the GitLab page of my main project to clone it : 

![](images/clone_link.png) 

![](images/cloning.png) 

Then, in order to synchronize the changes made, on the GitLab server, it was necessary to use some basic commands.

### 1.2.3. Important commands on Git Bash

| Command |  Purpose    | 
|-----|-----------------|
| git pull | download the last version of the project    | 
| git add -A | add all new modifications    |
| git status  |   inform about the status of the working tree   |
| git commit -m "comment"   | allow to comment the new modification(s)   |
| git push origin main | send the new version on the GitLab server  |

An example of the use of these commands is shown in the following figures : 

![1) Use of the "git status" command to see the status of the working tree and use of the command "git add -A" to add all the new modifications.](images/screen1.png) 

![2) Use of the command "git status" again and "git commit -m" to comment the modification(s).](images/screen2.png) 

![3) Use of the command "git status" again and "git push origin main" to send to the server.](images/screen3.png)

### 1.2.4. HTML VS Markdown

In order to put this documentation in the form of a web page, it is possible to either write it in HTML (not very practical to use and not easy to read) or to write it in Markdown (easier to use and to read). It is the 2nd option that will be used, the documentation will then be exported in HTML. 

To write the documentation in Markdown, a text editor is necessary. The one I use is _Visual studio code_. In order to learn how to use it and to learn the basic commands, I used [this guide](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#documenting-your-work-why-tomorrow-you-will-not-remember-anything) and [this tutorial](https://www.markdowntutorial.com/).  

Here's an example of some commands and text in Markdown : 

![](images/example.png)

### 1.2.5. Pictures and figures 

In order to add photos to the text, it is necessary to compress them and adjust their size. To do so, some programs such as GIMP ([explained in this link](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg)) can be used. However, I preferred to use a program often forgotten and yet easy to use : Paint. 

The latter allows the addition of shapes, text, resizing of images, etc. An example of resizing is shown in the following image : 

![](images/paint1.png) ![](images/paint2.png)

By clicking on resize, it is possible to change the percentage of the photo or the number of pixels. In order not to deform it , it is then possible to select the option "keep the proportions". 








