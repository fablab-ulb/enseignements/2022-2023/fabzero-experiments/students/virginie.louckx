# 3. 3D printing

## 3.1. Benefits and limitations of 3D printing

This week, we learned how to identify the benefits and limitations of 3D printing as well as how to apply design methods and production processes using 3D printing. 

During the weekend I had the opportunity to have legos at the disposal so I was able to build a construction inspired by [the flexlinks models shown in class](https://www.compliantmechanisms.byu.edu/flexlinks) :

![](images/lego1.png) ![](images/lego2.png) ![](images/lego3.png)

From this construction that will serve as a catapult, I was able to get a clear idea of the Flexlinks kit I needed. I then needed 2 Flexlinks models. 

### 3.1.1. Modification of the first FlexLink

After considering the use of flexlinks better, I decided to modify the first one that I designed ([see module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/fabzero-modules/module02/#first-flexlink)) slightly. In order to add a center bar to it, I added the following lines to the first Flexlink's code : 

```
\\ FILE : Flexlink_3.scad 

\\ AUTHOR : Virginie Louckx

\\ LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

//modif pour barre perpendiculaire
long_barre_perpend=25;
width_barre_perpend=2.5;
dist_x = 0;
dist_y = 2;

barre_perpend();
module barre_perpend() {
    translate([dist_x,dist_y,0]) cube([long_barre_perpend,width_barre_perpend,height], center = true);
}

```
It was then possible to change the position of a perpendicular bar along the main bar, on either side of the main bar, as well as its length and thickness. I also reduced the number of holes (also a parameter that can be changed) to give it more flexibility. I then proceeded to add fillets as shown in [the Computer-Aided Design (CAD) section](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/fabzero-modules/module02/#first-flexlink).

The flexlink then appeared as this : 

![](images/modif_flexlink1.png)



### 3.1.2. Second FlexLink

For this part, I wanted to be able to play on the following parameters : the radius, the opening angle between the 2 extremities of the part, its height (same parameter as for the previous flexlink), its thickness as well as the internal and external diameters of the ends. 
The following schematic represents the different parameters of the form : 

![](images/flexlink2_dim.png)

In order to produce this form, I wrote the following code in OpenSCAD : 

```

\\ FILE : Flexlink_2.scad 

\\ AUTHOR : Virginie Louckx

\\ LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

$fn=300;
R=32;
theta=90;
D=7;
d=5;
height=2;
e=0.5;

module cyl()
{
    union() {
        translate([0,R,0]) cylinder(h=height, r =D/2,center=true);;
        translate([R*sin(theta),R*cos(theta),0]) cylinder(h=height, r =D/2,center=true);;
    }
}

module arc_complet() {
    difference() {
        cylinder(h=height,r=R+e/2,center=true);
        cylinder(h=height+1,r=R-e/2,center=true);
    }
}


module arc() {
    difference() {
    difference() {
        arc_complet();
        translate([-R-5,0,0]) cube(2*R+10,2*R+10,height+1, center=true);
    }
    rotate(-theta+180) translate([-R-5,0,0]) cube(2*R+10,2*R+10,height+1, center=true);
    };
}

module ensemble() {
    union() {
        arc();
        cyl();
    }
}

flexlink();

module flexlink() {
    difference() {
    difference() {
        ensemble();
        translate([0,R,0]) cylinder(h=height+1, r =d/2,center=true);
    }
    translate([R*sin(theta),R*cos(theta),0]) cylinder(h=height+1, r =d/2,center=true);
    }
}

```

As for the first flexlink, I obtained the desired shape in OpenSCAD and then I modified it in FreeCAD in order to add fillets : 

![](images/flexlink2_op.png) ![](images/flexlink2_free.png)

My Flexlinks being modelled, I needed to test print and modify the right parameters to get the right kit. 

## 3.2. 3D printing session 

The first step for this session was to install PrusaSlicer on our computer. I downloaded the program [here](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.176205037.954279970.1647372356-1735415410.1647372356). After reading and reviewing [the 3D printing tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) in class I was able to prepare the gcode to print my Flexlinks kit.

### 3.2.1. First 3D printing attempt

My first attempt was not conclusive. Indeed, I had selected the wrong 3D printer in PrusaSlicer before getting the G-code. During the printing, the print head was too low and the resin was ripped off by it. Indeed, the right printer to select in PrusaSlicer was the following : 

![](images/imp.png) 


### 3.2.2. Second attempt

After selecting the right 3D printer, I was able to launch a successful first print. However, as explained in class, the scale of measurements is not precisely respected during printing, so my first successful attempt was not to scale to clip to a lego piece. This test also made me realize that the Flexlink 1 was much too thick and therefore not very flexible and that the Flexlink 2 was too thin and of too low amplitude. 

![](images/es1.png) ![](images/es1_2.png) 


In order to solve the problem of scale, in a student group, a lego bar was printed with different sizes of diameters evolving by steps of 0.1 mm. This allowed us to determine the right diameter value (which was 5.1mm) to introduce in our OpenSCAD code in order to obtain the lego scale. For the problem of dimensions, I had to recalculate them. I used a 72mm middle length for the first Flexlink and a 120° angle for the second Flexlink. I also decreased the thickness of the Flexlink1 and increased that of the 2nd. 

![](images/es3.png) 

### 3.2.3. Final attempt

After I realized that some of the legos pieces of my catapult had disappeared in the FabLab, I had to adapt it and my Flexlinks.
To do so, I built a new simpler catapult and cut a part of the first Flexlink part as shown in the figures below. 

![](images/cat1.png) ![](images/cat2.png) ![](images/cat3.png) 

![](images/cut.png) 



The video below shows the final results that are quite satisfying !


<iframe src="../images/vid.mp4" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe> 