# 2. Computer-Aided Design (CAD)

## 2.1. Third class

This week's assignment is to design, model and document a FlexLinks parametric construction kit that will be manufactured by 3D printers. The model should be parametric so that adjustments can be made taking into account material properties and machine characteristics.


### 2.1.1. Compliant mechanism and flexures

#### 2.1.1.1. What is a compliant mechanism?

A compliant mechanism is a flexible mechanism that transmits force and motion through the elastic deformation of the structure. It gets its motion from the relative flexibility of its members. Indeed, it is possible to deform a structure by imposing it one or more constraints. The deformation is then limited by the number of degrees of freedom (of rotation and translation), by the rigidity and the geometry of the structure. 

### Evaluation and selection of a 3D software

Since the 3D printer cannot read images composed of points, it is essential to work with vector images. Indeed, in the case of the latter, the printer will be able to read and follow the path described by the structure. To do this, 2 softwares can be used: _OpenSCAD_ and _FreeCAD_. I personally opted for the OpenSCAD program because it uses code and was therefore easier to parameterize the 3D objects. 

#### 2.1.1.2. OpenSCAD

This software allows to build 3D structures from a code. To start a new project, you need to open OpenSCAD, click on "new" and write the code of the desired 3D shape in the editor. 

![](images/openscad1.png) 

It is then possible to refer to a very useful [sheat sheet](https://openscad.org/cheatsheet/) to learn how to use the basic commands in OpenSCAD. The following are the commands that will be used the most : 

![](images/2D.png) ![](images/3D.png) ![](images/bool.png) 

![](images/transf.png) 

#### **Some examples in OpenSCAD**

Let's start by modeling a polyhedron with a triangular base (a 3-sided sphere): 

![](images/poly_tri_code.png)

![](images/poly_tri.png) 

Let's put a sphere in it using the command _union()_ and let's define the parameter "side": 

![](images/w_sphere_code.png)

![](images/w_sphere.png)

Now let's use de command _hull()_. The latter will create a convex shell from the objects it contains. This action is like wrapping them in plastic film : 

![](images/hull_code.png)

![](images/hull.png)

Since we have parameterized the model by defining the "side" parameter, we can easily modify it : 


![](images/hull_mod.png)

![](images/hull_mod_6.png)

#### 2.1.1.3. FreeCAD

I still used the FreeCAD program to add fillets to my 3D objects afterwards. These steps are explained later in this documentation. 

### 2.1.2. Demonstration and description of the processes used in modeling with a 3D software

#### 2.1.2.1. First FlexLink


In order to create my first FlexLink module I started from Pauline Mackelbert's code (taken from Alexandre Stoesser).
I modified it to be more **functional** and **parametric** : 

The following schematics show the parameters that I added that can be easily modified in the code :

![](images/schéma_param.png)
![](images/schéma_param1.png)


The associated code to this figure is given below.

```

\\ FILE : Flexlink_1.scad 

\\ AUTHOR : Virginie Louckx

\\ DATE OF MODIFICATION : 25/03/2023

\\ FROM : Pauline Mackelbert

\\ LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)


$fn=50;
nbr_trous = 2;

height = 3;
middle_length = 30;
center_holes_distance = 8;
width_edge = 1;
diameter_hole = 5;
structure_width = diameter_hole+width_edge*2;


width_bar=1;

x0=0;x1=0;x2=0;
z0=0; z1=0;z2=0;
 
y0=middle_length/2+center_holes_distance/2+structure_width/2; 
y1=y0+center_holes_distance/2;  
y2=y0-center_holes_distance/2;  



union(){
    
cube([width_bar,middle_length,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole);


}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole){

    difference(){
    if(nbr_trous==1){
		rectanglearrondi(x0,y0-center_holes_distance/2-0.1,z0,center_holes_distance*0, structure_width, height);
    }
        
    if (nbr_trous==2) {
		rectanglearrondi(x0,y0-0.1,z0,center_holes_distance, structure_width, height);
    }
        
        
    if (nbr_trous == 1){
		translate([x1,y0-center_holes_distance/2,z1]) cylinder(height+0.1, d= diameter_hole, center=true);
    }
    if (nbr_trous == 2){
        translate([x1,y1,z1]) cylinder(height+0.1, d= diameter_hole, center=true);
        translate([x2,y2,z2]) cylinder(height+0.1, d= diameter_hole, center=true);
    }
    }
}

module rectanglearrondi(x0,y0,z0,center_holes_distance, structure_width, height){

    union(){
    translate([x0,y0,z0]) cube([structure_width,center_holes_distance+1, height], center=true);

    translate([x0,y0-(center_holes_distance)/2,z0])cylinder(height, d=structure_width, center=true);

    translate([x0,y0+(center_holes_distance)/2,z0])cylinder(height, d=structure_width, center=true);
    }
}
```

This code gives the following Flexlink module :

![](images/forme1.png)

Besides, the code allows to choose between 1 hole and two holes with the parameter nbr_trous. By choosing nbr_trous = 1 we obtain :

![](images/forme2.png)

I then modified the first shape with two holes using the FreeCAD program to add "fillets" to some of the edges. 

To do this, I opened FreeCad, created a new file and imported my previously created OpenSCAD model :

![](images/import.png) 

The model appears with the possibility to select the edges. After having selected the 4 edges to be modified, I choose the "part" section of the workbench :

![](images/modèle_free_flèches.png) 

![](images/part.png) 

I then clicked on the " part tools " in the toolbar and selected " fillet " :

![](images/outils_pièce.png) 

![](images/congés.png) 

The taskbar then displays the list of selected edges and the radius of curvature that you can choose. The only thing left to do is to click on "ok"!

![](images/vue_combinée.png) 

The "fillets" now appear on the form :

![](images/chanfrein.png)

The Flexlinks parts can now be printed with 3D printers. This will be the objective of the third module.

### 2.1.3. Licenses 

[_Creative Commons licenses give everyone from individual creators to large institutions a standardized way to grant the public permission to use their creative work under copyright law. From the reuser’s perspective, the presence of a Creative Commons license on a copyrighted work answers the question, “What can I do with this work?”_](https://creativecommons.org/about/cclicenses/).

Indeed, in order to allow others to use our documentation, it is necessary to apply a creative common license to it. As the link above indicates, there are different creative commons licenses that inform about the legal uses of a document. 

In order to indicate the CC license I use for my codes, I have clearly indicated at the beginning of them the license I use as well as the link corresponding to it : 

```
\\ FILE : Flexlink_2.scad 

\\ AUTHOR : Virginie Louckx

\\ LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

```
