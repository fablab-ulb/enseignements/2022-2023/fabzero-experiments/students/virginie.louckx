$fn=300;
R=32;
theta=60;
D=7;
d=5;
height=2;
e=0.5;


module cyl()
{
    union() {
        translate([0,R,0]) cylinder(h=height, r =D/2,center=true);;
        translate([R*sin(theta),R*cos(theta),0]) cylinder(h=height, r =D/2,center=true);;
    }
}

module arc_complet() {
    difference() {
        cylinder(h=height,r=R+e/2,center=true);
        cylinder(h=height+1,r=R-e/2,center=true);
    }
}


module arc() {
    difference() {
    difference() {
        arc_complet();
        translate([-R-5,0,0]) cube(2*R+10,2*R+10,height+1, center=true);
    }
    rotate(-theta+180) translate([-R-5,0,0]) cube(2*R+10,2*R+10,height+1, center=true);
    };
}

module ensemble() {
    union() {
        arc();
        cyl();
    }
}

flexlink();

module flexlink() {
    difference() {
    difference() {
        ensemble();
        translate([0,R,0]) cylinder(h=height+1, r =d/2,center=true);
    }
    translate([R*sin(theta),R*cos(theta),0]) cylinder(h=height+1, r =d/2,center=true);
    };
}

