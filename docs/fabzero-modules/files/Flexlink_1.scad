/*

FILE : flexlink_lego_3.scad

AUTHOR : Pauline Mackelbert

DATE OF MODIFICATION : 28/03/2022

FROM : Alexandre Stoesser, Flexible3Dv2.scad last modified on 22/03/2022 (https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alex.stoesser/-/blob/main/docs/Files/Flexible3Dv2.scad)

LICENSE : Creative Commons Attribution 4.0 [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

*/

$fn=50;
nbr_trous = 2;

height = 3;
middle_length = 30;
center_holes_distance = 8;
width_edge = 1;
diameter_hole = 5;
structure_width = diameter_hole+width_edge*2;


width_bar=1;

x0=0;x1=0;x2=0;
z0=0; z1=0;z2=0;
 
y0=middle_length/2+center_holes_distance/2+structure_width/2; 
y1=y0+center_holes_distance/2;  
y2=y0-center_holes_distance/2;  



union(){
    
cube([width_bar,middle_length,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole);


}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole){

    difference(){
    if(nbr_trous==1){
		rectanglearrondi(x0,y0-center_holes_distance/2-0.1,z0,center_holes_distance*0, structure_width, height);
    }
        
    if (nbr_trous==2) {
		rectanglearrondi(x0,y0-0.1,z0,center_holes_distance, structure_width, height);
    }
        
        
    if (nbr_trous == 1){
		translate([x1,y0-center_holes_distance/2,z1]) cylinder(height+0.1, d= diameter_hole, center=true);
    }
    if (nbr_trous == 2){
        translate([x1,y1,z1]) cylinder(height+0.1, d= diameter_hole, center=true);
        translate([x2,y2,z2]) cylinder(height+0.1, d= diameter_hole, center=true);
    }
    }
}

module rectanglearrondi(x0,y0,z0,center_holes_distance, structure_width, height){

    union(){
    translate([x0,y0,z0]) cube([structure_width,center_holes_distance+1, height], center=true);

    translate([x0,y0-(center_holes_distance)/2,z0])cylinder(height, d=structure_width, center=true);

    translate([x0,y0+(center_holes_distance)/2,z0])cylinder(height, d=structure_width, center=true);
    }
}









































