# 5. Group dynamics and final project

For the sessions concerning group dynamics and the final project, we proceeded as follows 

1) A first session to learn about problem and objective tree analysis. 
2) A second session to form the final project group.
3) A third session to train us in project management.
4) A fourth session to converge on a concrete problem for our final project.

## 5.1 Project Analysis and Design

In the first session, we learned what a problem tree and an objective tree are. The concept is as follows : a problem tree consists of placing the main problem at its center (the trunk). It is then possible to identify the causes of this main problem: these are the roots of the tree. As for the branches, they represent the consequences linked to the main problem. The tree can then be more precise and present sub-branches and sub-roots which represent respectively the secondary effects and the deeper causes. 

To build an objective tree it is possible to start from the problem tree. It is sufficient to reformulate the main problem into the main objective (trunk), the causes into actions leading to the objective (roots) and the consequences into results (branches). [This video](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY) explains the concept very well.

For the assignment, we had to take inspiration from fablab projects and frugal science projects presented in class and choose a project that we care about. 
Then, we were asked to build the problem tree and the objective tree of this project. 

I chose the project "PlanktoScope" whose web page can be found on [this link](https://www.planktoscope.org/). This project consists in the construction of an affordable microscope, accessible to all, compact and composed of simple modules in order to study the phytoplankton. I particularly liked this project because the subject fascinates me and the documentation concerning the construction of the device and its results allows a better understanding and knowledge of phytoplankton which is at the foundation of the system of life.

The problem and objective trees of this project are given below :  

|Problem Tree|Objective Tree|
|---|---|
|![](images/problem_tree.png)|![](images/objective_tree.png)|


## 5.2 Group formation and brainstorming on project problems

During the second session, we had to first of all put together objects that represented a problematic that we liked. After having placed all our objects on the floor, the people who had similar objects or themes that could be put in common, gathered to form the final project groups. 

For our group, we had the following objects: 

| Person | Object | 
|-----|-----------------|
| [Aymane](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/aymane.taifour/) | public transportation ticket | 
| [Lorea](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/) | 1€ coin|
| [Mariam](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/) | small car |
| [Matthew](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/) | label from a soy yogurt cup |
| [Virginie](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/) | car key |

We grouped together because we thought our items were similar and related to one or more transportation issues. In order to better understand what each of us had in mind, we each wrote the idea behind our object on post-it notes: 

| Person | Object | Thematic behind the object|
|-----|--------------|------------|
|[Aymane](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/aymane.taifour/) | public transportation ticket | energy cost of transportation|
| [Lorea](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/) | 1€ coin | control of industries|
| [Mariam](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/) | small car | transportation pollution
| [Matthew](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/) |label from a soy yogurt cup |Transportation pollution
| [Virginie](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/) |car key | energy pollution (also transportation-related)


After that, we proceeded to a decisional positioning which consists in placing one's name on an axis going from "I don't agree to work on this theme" to "I am very enthusiastic to work on this theme" in order to determine which theme was convenient for all of us. The theme that emerged from this decision positioning was transportation-related pollution.
Once we had chosen our theme, we tried to dissect it by looking for the underlying issues. The topics that emerged were the following: 

1) Commercial transportation
2) The cost and pollution of fossil fuels 
3) Air pollution and its consequences on humans 
4) Shortage of raw materials 
5) Lack of education and information on these subjects
6) High prices of local agriculture 
7) Increasingly frequent and often individual travel 
8) Noise pollution 
9) Useless and excessive transportation 
10) Loss of biodiversity

Etc. 

This allowed us to have a set of ideas for our final topic. 

## 5.3 Group dynamics 

In this session, we were introduced to project management by studying different tools. In this documentation, I will develop 3 tools that I found interesting and useful to manage our final project. 

### 5.3.1. First project management tool : The finger technique 

This technique allows you to make a turn to speak within a group in a fairly equitable way. When someone is speaking and another person wants to speak, that person will point to the table with his or her index finger to show that he or she wants to speak second. If a third person wishes to speak after this one, he or she will put two fingers on the table and so on. This allows the person speaking to remember that other people wish to speak and thus to finish what he or she is saying while giving the discussion a rhythm that respects everyone's ideas. 

### 5.3.2. Second project management tool: The division of tasks and roles 

Several ideas for dividing up tasks and roles came up and inspired us: 

1) Divide the tasks according to the tools that everyone has learned and the skills of each person. For example, if our final project requires the use of a laser cutter and a microcontroller, the people who have had these formations respectively will focus on the development of tasks where the use of these tools is necessary. 

2) Break down large tasks into operational subtasks. Indeed, some tasks that are too important lack the information to be carried out directly, so it is interesting to break them down into subtasks that can be carried out more quickly in order to keep moving forward. 

3) Assign the roles of animator, secretary, communication and time management to the group. In order to determine who does what, an interesting method is the motivated fingers method. This consists of raising your hand with a number of fingers representing the motivation you have for the proposed role. 

### 5.3.3. Third project management tool: Structuring our meetings

Knowing how to structure meetings in project management is essential to be efficient, to move forward quickly and to keep a good group cohesion. To do so, the structure presented in the course was the following: 

1) Make a entrance weather at the beginning 
2) Give the roles of the day
3) Set the agenda 
4) Taking stock 
5) Agenda and distribution of future roles/tasks
6) Make an exit weather  
7) Closing the meeting 

During these meetings, we will be led to make decisions. In order to do this, several methods were discovered in class such as consensus, weighted/ranked voting, pro/con, coin toss, majority judgment, no-objection management, and the temperature check method. 

As a group, we determined that it would be best to use two of the above methods for important and less important decisions: 

|Important decisions|Less important decisions|
|-----|------|
|Weighted vote|Temperature check|
|This is a method of distributing a total of 3 votes on the idea(s) you prefer. |This methode involves raising your hand at 3 different levels (high = strongly agree, middle = +/- agree, low = disagree) to indicate your enthusiasm for a proposed idea. |

## 5.4 Problem and objective of your group project

The objective of fourth session was to converge on a concrete problem for our final project.

To do so, we proceeded in 3 steps: 

### 5.4.1. First step: write each on a board the 3 topics that interest us the most that came up during the first session :

![](images/panneau1.png)

While doing this, we realized that the common point of these topics was the loss of biodiversity linked to various types of pollution. 

Among these, water pollution was a point that particularly spoke to us. So we placed "Water pollution" as the main topic and determined the different causes of it. 

### 5.4.2. Second step: schematization of our problem in the form of a tree

We proceeded to the schematization of our problem in the form of a tree, with the causes as roots and the consequences as branches. This allowed us to have a better global idea of the problem and to find more precise ideas for our final project. 

![](images/panneau2.png)

### 5.4.3. Third step : Choice constraint 

Since our main problem was still too vague, we proceeded to apply a choice constraint in order to impose a direction to our research. 

To do this, we chose the constraint of a place. After some research on the internet, the river Senne seems to be one of the most polluted in Belgium. 

We therefore replaced our problem of "water pollution" by that of "pollution of the Senne" in our tree. We then looked for new causes and consequences to this one which led us to this schematic : 

![](images/panneau3.png)





