## Foreword

Hello everyone!

This is my student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

## About me

![](images/profile.png) 

Hi! I am Virginie Louckx. I am a master student in bioengineering in the section of environmental sciences and technology. I am very motivated to learn new skills with the course "How to make (almost) any experiments using digital fabrication" and to deepen some notions I have acquired these last years! 

Visit this website to see my work! ![](images/emoji.png) 

## My background

I was born in Brussels and did my secondary education there. I then moved to Hainaut a few years before starting my university studies at ULB. I am passionate about science, nature and the environment, so studying Bioengineering was an obvious choice.

## Previous work

​Through the various subjects covered during my bachelor's degree and the beginning of my master's degree, I discovered new passions such as microbiology, physiology, everything related to biotechnology but also cosmetics and pharmaceuticals. 

I was able to discover this during group projects that I particularly liked such as : 

### A solution to mass-produce graphene at low cost and with reduced environmental impact 

Which was the study and comparison of three graphene production methods at industrial scale with low environmental
impact and low cost.

### Arabidopsis Thalianna 

Which consisted of a laboratory study of the morphological and physiological adaptations of the Arabidopsis Thalianna model according to its climate.

![](images/arabi.png) ![](images/arabi2.png)

### Farm projet 

Which consisted of an interview with an agricultural exploitation owners and of a technical report writing regarding the farm functioning, the management and the challenges from an agricultural and financial point of view.

![](images/vache.gif)